#ifndef PARSER_H
#define PARSER_H

#include <QDialog>
#include "exprtk.hpp"
#include "help.h"
#include <QDebug>

namespace Ui {
class Parser;
}

class Parser : public QDialog
{
    Q_OBJECT

public:
    explicit Parser(QWidget *parent = 0);
    ~Parser();


signals:
    void FunctionLoaded(std::string);

private slots:
    void on_Help_clicked();

    void on_buttonBox_accepted();

private:
    Ui::Parser *ui;
};

#endif // PARSER_H
