#ifndef INTERFACE_H
#define INTERFACE_H
#include <QObject>
#include <QMainWindow>
#include <QDebug>

#include "qcustomplot.h"
#include "sympleks.h"
#include "parser.h"
#include "exprtk.hpp"
#include "algorytm.h"
#include <limits.h>

namespace Ui {
class Interface;
}

class Interface : public QMainWindow
{
    Q_OBJECT

public:
    explicit Interface(QWidget *parent = 0);
    ~Interface();
    double eps;
    double alfa;
    double beta;
    double gamma;
    double sigma;
    int iter_amount;
    algorytm* min_search;
    int Type;
    Sympleks* Symp;
    std::string expr_string;
    Parser okienko;
    bool min_x_set [5];
    bool max_x_set [5];
    double min_x [5];
    double max_x [5];
    void parsowanie();
    QString createLog();


    typedef exprtk::symbol_table<double> symbol_table_t;
    typedef exprtk::expression<double>     expression_t;
    typedef exprtk::parser<double>             parser_t;


    symbol_table_t symbol_table;
    expression_t expression;
    parser_t parser;

    double x1=0;
    double x2=0;
    double x3=0;
    double x4=0;
    double x5=0;


private slots:
    void on_StepSlider_sliderMoved(int position);

    void on_LoadFunction_clicked();

    void on_Start_clicked();

    void on_Stop_clicked();

    void on_EditAlfa_textEdited(const QString &arg1);

    void on_EditBeta_textEdited(const QString &arg1);

    void on_EditGamma_textEdited(const QString &arg1);

    void on_EditEpsilon_textEdited(const QString &arg1);

    void on_EditSigma_textEdited(const QString &arg1);

    void on_IterationAmount_valueChanged(int arg1);

    void on_FunctionLoaded(std::string function);

    void on_min_x1_editingFinished();

    void on_min_x2_editingFinished();

    void on_min_x3_editingFinished();

    void on_min_x4_editingFinished();

    void on_min_x5_editingFinished();


    void on_max_x1_editingFinished();

    void on_max_x2_editingFinished();

    void on_max_x3_editingFinished();

    void on_max_x4_editingFinished();

    void on_max_x5_editingFinished();


private:
    Ui::Interface *ui;

signals:
    void Stop_algorithm(int iter);
    void Start_algorithm(int iter);

};

#endif // INTERFACE_H
