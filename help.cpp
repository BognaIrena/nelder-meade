#include "help.h"
#include "ui_help.h"

Help::Help(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Help)
{
    ui->setupUi(this);
    setWindowTitle(tr("Dopuszczalne operatory"));
    QStringList operatory;
    operatory << "        OPERATORY PODSTAWOWE      "
              << " + lub +=         Dodawanie"
              << " - lub -=         Odejmowanie"
              << " * lub *=         Mnożenie"
              << " / lub /=         Dzielenie"
              << " % lub %=         Reszta z dzielenia"
              << " ^                Potęgowanie"
              << " :=               Przypisanie wartości"
              << " = lub ==         Równość"
              << " <> lub !=        Negacja równości"
              << " <                Mniejsze od"
              << " >                Większe od"
              << " <=               Mniejsze bądź równe"
              << " >=               Większe bądź równe"
              << "               FUNKCJE                "
              << " abs              Wartość bezwzględna"
              << " avg              Średnia z argumentów"
              << " ceil             Najmniejsza liczba całkowita >=x"
              << " clamp(a,x,b)     Zamknij x w przedziale (a,b)"
              << " equal            Test równości ze znormalizowanym epsilonem"
              << " erf              Funkcja błędu x"
              << " erfc             Komplementarna funkcja błędu x"
              << " exp              Eksponenta, e^x"
              << " floor            Najbliższa liczba całkowita <=x"
              << " frac             Część ułamkowa x"
              << " iclamp           Zamknięcie x poza przedziałem (a,b)"
              << " inrange          Sprawdzenie, czy x jest w przedziale (a,b)"
              << " log              Logarytm naturalny z x"
              << " log10            Logarytm o podstawie 10 z x"
              << " log2             Logarytm o podstawie 2 z x"
              << " logn(x,n)        Logarytm o podstawie n z x"
              << " max              Największy z argumentów"
              << " min              Najmniejszy z argumentów"
              << " mul              Mnożenie wszystkich argumentów"
              << " root(x,n)        Pierwiastek n-tego stopnia z x"
              << " roundn(x,n)      Zaokrąglenie x do n-tego miejsca po przecinku"
              << " sgn              Znak argumentu"
              << " sqrt             Pierwiastek kwadratowy z x"
              << " sum              Suma wszystkich argumentów"
              << " trunc            Część całkowita x"
              << "          FUNKCJE TRYGONOMETRYCZNE          "
              << " acos             Arcus cosinus"
              << " acosh            Arcus cosinusa hiperbolicznego"
              << " asin             Arcus sinus"
              << " asinh            Arcus sinus hiperboliczny"
              << " atan             Arcus tangens"
              << " atan2(x,y)       Arcus tangens x/y"
              << " cos              Cosinus"
              << " cosh             Cosinus hiperboliczny"
              << " cot              Cotangens"
              << " csc              Cosecans"
              << " sec              Secans"
              << " sin              Sinus"
              << " sinh             Sinus hiperboliczny"
              << " tan              Tangens"
              << " tanh             Tangens hiperboliczny"
              << " deg2rad          Przeliczenie ze stopni na radiany"
              << " deg2grad         Przeliczenie ze stopni na gradiany"
              << " rad2deg          Przeliczenie z radianów na stopnie"
              << " grad2deg         Przeliczenie z gradianów na stopnie"
              << "          OPERATORY LOGICZNE      "
              << " true             Prawda, 1"
              << " false            Fałsz, 0"
              << " and              Prawda, jeśli obie strony są prawdziwe"
              << " mand()           Prawda, jeśli wszystkie warunki są spełnione"
              << " mor()            Prawda, jeśli choć jeden warunek jest spełniony"
              << " nand             Prawda, jeśli jedna ze stron jest prawdziwa"
              << " nor              Prawda, jeśli or jest fałszywe"
              << " not              Negacja"
              << " or               Prawda, jeśli jedna ze stron jest prawdziwa"
              << " xor              Prawda, jeśli jedna strona jest prawdziwa, a druga fałszywa"
              << " xnor             Prawda, jeśli obie strony są prawdziwe lub fałszywe";

    ui->listWidget->addItems(operatory);

}

Help::~Help()
{
    delete ui;
}
