#-------------------------------------------------
#
# Project created by QtCreator 2015-06-06T16:29:32
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = Pelzaczek3
TEMPLATE = app

win32 {
QMAKE_CXXFLAGS += /bigobj
}

SOURCES += main.cpp\
        interface.cpp \
    help.cpp \
    parser.cpp \
    qcustomplot.cpp \
    sympleks.cpp \
    algorytm.cpp

HEADERS  += interface.h \
    exprtk.hpp \
    help.h \
    parser.h \
    qcustomplot.h \
    sympleks.h \
    algorytm.h

FORMS    += interface.ui \
    help.ui \
    parser.ui

DISTFILES += \
    Pelzaczek3.pro.user
