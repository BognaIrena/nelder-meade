#ifndef SYMPLEKS_H
#define SYMPLEKS_H

#include <QVector>
#include <vector>
#include <QDebug>
#include <chrono>
#include <random>

class Sympleks
{
public:
    Sympleks(int n);
    Sympleks();
    ~Sympleks();
    int num_of_points;
    int num_of_coordinates;
    typedef  QVector<double> coordinate;
    typedef  QVector<coordinate> point;
    QVector<point> iteration;
//  QVector<QVector<QVector<double>>> iteration;
    int min_constraint [5];
    int max_constraint [5];
    double min_x[2];
    double max_x[2];
    void initiate();
};

#endif // SYMPLEKS_H
