#ifndef ALGORYTM_H
#define ALGORYTM_H

#include <QDialog>
#include "sympleks.h"
#include <QVector>
#include <QDebug>
#include "exprtk.hpp"
#include <cmath>


class algorytm: public QDialog
{
    Q_OBJECT
    struct Point
    {
        int point_index;
        QVector <double> coords;
        double value;
    };

public:
    explicit algorytm(QWidget *parent = 0);
    algorytm(double a, double b, double g, double s, Sympleks* symp, int i);
    ~algorytm();
    std::string expr_string;
    Sympleks* Symp;
    QVector <double> symp_value;
    double alfa, beta, gamma, sigma;
    Point aH, aSH, aL, aR, aE, aC, sc;
    int iter;
    int iter_max;

    bool search_continue(double eps);
    void Nelder_Meade(double eps);

    typedef exprtk::symbol_table<double> symbol_table_t;
    typedef exprtk::expression<double>     expression_t;
    typedef exprtk::parser<double>             parser_t;


    symbol_table_t symbol_table;
    expression_t expression;
    parser_t parser;

    double x1=0;
    double x2=0;
    double x3=0;
    double x4=0;
    double x5=0;

//    template <typename T>
    void function_evaluation();
//    template <typename T>
    void symp_value_evaluation();
    void symetry_center();
    bool reflect();
    bool expand();
    bool contract_in();
    bool contract_out();
    void reduct();




};

#endif // ALGORYTM_H
