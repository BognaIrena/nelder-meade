#include "algorytm.h"
#define SQR(x) ((x)*(x))

algorytm::algorytm(QWidget *parent) :
QDialog(parent)
{

}

algorytm::algorytm(double a, double b, double g, double s, Sympleks* symp, int i)
{
    this->alfa=a;
    this->beta=b;
    this->gamma=g;
    this->sigma=s;
    this->Symp=symp;
    iter=0;
    iter_max=i;
}

algorytm::~algorytm()
{

}

bool algorytm::search_continue(double eps)
{
    double symp_size=0;
    // warunek stopu - suma kwadratów różnic porównana do kwadratu epsilona

    for(int i=0; i<Symp->num_of_coordinates; i++)
    {
        symp_size += SQR(aL.coords.value(i)-aH.coords.value(i));
    }


    if(symp_size>SQR(eps))
    {
        return 1;
    }
    else
    {
        return 0;
    }

}


void algorytm::Nelder_Meade(double eps)
{
    bool reflect_flag=false;
    bool expand_flag=false;
    bool contract_in_flag=false;
    bool contract_out_flag=false;

    function_evaluation();


    do
    {
        reflect_flag=false;
        expand_flag=false;
        contract_in_flag=false;
        contract_out_flag=false;
        //Przygototwanie nowej iteracji
        Symp->iteration.resize(iter+1);
        Symp->iteration.insert(iter+1, Symp->iteration.value(iter));

        symp_value_evaluation();
        symetry_center();

        reflect_flag=reflect();

        if(aR.value<aL.value)
        {
            expand_flag=expand();
        }


        if(aR.value>aSH.value)
        {
            if(aR.value>aH.value)
            {
                contract_in_flag=contract_in();

            }
            if(aR.value<aH.value)
            {
                contract_out_flag=contract_out();
            }
        }

        if(reflect_flag==false && expand_flag==false && contract_in_flag==false && contract_out_flag==false)
        {
            reduct();
        }



        iter++;
//        qDebug()<<"Iteracja " << iter << " zakończona z:\n"
//               <<"odbicie: " << reflect_flag
//              <<"\nekspansja: " << expand_flag
//             <<"\nkontrakcja out: " << contract_out_flag
//            << "\nkontrakcja in: " << contract_in_flag
//            << "\nnajlepszy punkt: " << aL.value;

    } while(search_continue(eps) && iter<=iter_max);


    if(iter<=iter_max)
    {
        qDebug()<<"Znaleziono minimum funkcji!\nLiczba iteracji: " << iter
                <<"\nWartość funkcji: " << aL.value
                <<"\nPołożenie: " << aL.coords <<" min "<<Symp->min_x[0]
               << " max " << Symp->max_x[0];
    }
    else
    {
        qDebug()<<"Osiągnięto maksymalną liczbę iteracji nie znajdując minimum funkcji."
               <<"Najlepszy dotychczas znaleziony punkt: "<< aL.value <<endl
               <<"Położenie: " << aL.coords;
    }

}


void algorytm::function_evaluation()
{
    symbol_table.add_variable("x1",x1);
    if(Symp->num_of_coordinates>=2)
    {
        symbol_table.add_variable("x2",x2);
        if(Symp->num_of_coordinates>=3)
        {
            symbol_table.add_variable("x3",x3);
            if(Symp->num_of_coordinates>=4)
            {
                symbol_table.add_variable("x4",x4);
                if(Symp->num_of_coordinates==5)
                {
                    symbol_table.add_variable("x5",x5);
                }
            }
        }
    }

    symbol_table.add_constants();

    expression.register_symbol_table(symbol_table);

    parser.compile(this->expr_string, expression);
}



void algorytm::symp_value_evaluation()
{
    QVector<double> x;
    double val;

    symp_value.clear();

    for (int i=0; i<this->Symp->num_of_points; i++)
    {
        x.clear();

        x1=Symp->iteration.value(iter).value(i).value(0);
        x.append(x1);

        if(Symp->num_of_coordinates>=2)
        {
            x2=Symp->iteration.value(iter).value(i).value(1);
            x.append(x2);

            if(Symp->num_of_coordinates>=3)
            {
                x3=Symp->iteration.value(iter).value(i).value(2);
                x.append(x3);

                if(Symp->num_of_coordinates>=4)
                {
                    x4=Symp->iteration.value(iter).value(i).value(3);
                    x.append(x4);

                    if(Symp->num_of_coordinates==5)
                    {
                        x5=Symp->iteration.value(iter).value(i).value(4);
                        x.append(x5);
                    }
                }
            }
        }
        val=expression.value();
        symp_value.append(val);


        if (i==0)
        {
            aH.point_index=0;
            aSH.point_index=0;
            aL.point_index=0;
            aH.value=val;
            aSH.value=val;
            aL.value=val;
            aH.coords=x;
            aSH.coords=x;
            aL.coords=x;
            if(iter==0)
            {
                Symp->min_x[0]=x[0];
                Symp->min_x[1]=x[1];
                Symp->max_x[0]=x[0];
                Symp->max_x[1]=x[1];
            }
        }

        if (val>aH.value)
        {
            aSH.value=aH.value;
            aSH.point_index=aH.point_index;
            aSH.coords=aH.coords;
            aH.value=val;
            aH.point_index=i;
            aH.coords=x;
        }

        if (val>aSH.value && val<aH.value)
        {
            aSH.value=val;
            aSH.point_index=i;
            aSH.coords=x;
        }
        if (val<aL.value)
        {
            aL.value=val;
            aL.point_index=i;
            aL.coords=x;
        }
        if(x[0]<Symp->min_x[0])
        {   Symp->min_x[0]=x[0];    }
        if(x[0]>Symp->max_x[0])
        {   Symp->max_x[0]=x[0];    }
        if(x[1]<Symp->min_x[1])
        {   Symp->min_x[1]=x[1];    }
        if(x[1]>Symp->max_x[1])
        {   Symp->max_x[1]=x[1];    }
    }

}

void algorytm::symetry_center()
{
    double tmp1=0;
    double tmp2=0;
    double tmp3=0;
    double tmp4=0;
    double tmp5=0;

    for(int i=0; i<Symp->num_of_points; i++)
    {
        if (i!=aH.point_index)
        {
            tmp1+=Symp->iteration.value(iter).value(i).value(0);

            if(Symp->num_of_coordinates>=2)
            {
                tmp2+=Symp->iteration.value(iter).value(i).value(1);

                if(Symp->num_of_coordinates>=3)
                {
                    tmp3+=Symp->iteration.value(iter).value(i).value(2);

                    if(Symp->num_of_coordinates>=4)
                    {
                        tmp4+=Symp->iteration.value(iter).value(i).value(3);

                        if(Symp->num_of_coordinates==5)
                        {
                            tmp5+=Symp->iteration.value(iter).value(i).value(4);
                        }
                    }
                }
            }
        }
    }

    tmp1=tmp1/(Symp->num_of_points-1);
    tmp2=tmp2/(Symp->num_of_points-1);
    tmp3=tmp3/(Symp->num_of_points-1);
    tmp4=tmp4/(Symp->num_of_points-1);
    tmp5=tmp5/(Symp->num_of_points-1);

    x1=tmp1;

    if(Symp->num_of_coordinates>=2)
    {
        x2=tmp2;
        if(Symp->num_of_coordinates>=3)
        {
            x3=tmp3;
            if(Symp->num_of_coordinates>=4)
            {
                x4=tmp4;
                if(Symp->num_of_coordinates==5)
                {
                    x5=tmp5;
                }
            }
        }
    }
    sc.value=expression.value();

    sc.coords.clear();
    sc.coords.append(tmp1);
    sc.coords.append(tmp2);
    sc.coords.append(tmp3);
    sc.coords.append(tmp4);
    sc.coords.append(tmp5);
}

bool algorytm::reflect()
{
    aR.coords.clear();
    aR.coords.append(sc.coords.value(0)+alfa*(sc.coords.value(0)-aH.coords.value(0)));
    x1=aR.coords.value(0);
    if(Symp->num_of_coordinates>=2)
    {
        aR.coords.append(sc.coords.value(1)+alfa*(sc.coords.value(1)-aH.coords.value(1)));
        x2=aR.coords.value(1);
        if(Symp->num_of_coordinates>=3)
        {
            aR.coords.append(sc.coords.value(2)+alfa*(sc.coords.value(2)-aH.coords.value(2)));
            x3=aR.coords.value(2);
            if(Symp->num_of_coordinates>=4)
            {
                aR.coords.append(sc.coords.value(3)+alfa*(sc.coords.value(3)-aH.coords.value(3)));
                x4=aR.coords.value(3);
                if(Symp->num_of_coordinates==5)
                {
                    aR.coords.append(sc.coords.value(4)+alfa*(sc.coords.value(4)-aH.coords.value(4)));
                    x5=aR.coords.value(4);
                }
            }
        }
    }
    aR.value=expression.value();
    aR.point_index=aH.point_index;

    if(aL.value<aR.value && aR.value<aSH.value)
    {
        Symp->iteration[iter+1][aH.point_index]=aR.coords;
        if(aR.coords[0]<Symp->min_x[0])
        {   Symp->min_x[0]=aR.coords[0];    }
        if(aR.coords[0]>Symp->max_x[0])
        {   Symp->max_x[0]=aR.coords[0];    }
        if(aR.coords[1]<Symp->min_x[1])
        {   Symp->min_x[1]=aR.coords[1];    }
        if(aR.coords[1]>Symp->max_x[1])
        {   Symp->max_x[1]=aR.coords[1];    }
        return 1;
    }
    else
    {
        return 0;
    }
}


bool algorytm::expand()
{
    aE.coords.clear();
    aE.coords.append(sc.coords.value(0)+gamma*(aR.coords.value(0)-sc.coords.value(0)));
    x1=aE.coords.value(0);

    if(Symp->num_of_coordinates>=2)
    {
        aE.coords.append(sc.coords.value(1)+gamma*(aR.coords.value(1)-sc.coords.value(1)));
        x2=aE.coords.value(1);
        if(Symp->num_of_coordinates>=3)
        {
            aE.coords.append(sc.coords.value(2)+gamma*(aR.coords.value(2)-sc.coords.value(2)));
            x3=aE.coords.value(2);
            if(Symp->num_of_coordinates>=4)
            {
                aE.coords.append(sc.coords.value(3)+gamma*(aR.coords.value(3)-sc.coords.value(3)));
                x4=aE.coords.value(3);
                if(Symp->num_of_coordinates==5)
                {
                    aE.coords.append(sc.coords.value(4)+gamma*(aR.coords.value(4)-sc.coords.value(4)));
                    x5=aE.coords.value(4);
                }
            }
        }
    }
    aE.value=expression.value();

    if(aE.value<aR.value && aR.value<aL.value)
    {
        aE.point_index=aR.point_index;
        Symp->iteration[iter+1][aH.point_index] = aE.coords;
        if(aE.coords[0]<Symp->min_x[0])
        {   Symp->min_x[0]=aE.coords[0];    }
        if(aE.coords[0]>Symp->max_x[0])
        {   Symp->max_x[0]=aE.coords[0];    }
        if(aE.coords[1]<Symp->min_x[1])
        {   Symp->min_x[1]=aE.coords[1];    }
        if(aE.coords[1]>Symp->max_x[1])
        {   Symp->max_x[1]=aE.coords[1];    }
        return 1;
    }
    else
    {
        return 0;
    }

}

bool algorytm::contract_in()
{
    aC.coords.clear();
    aC.coords.append(sc.coords.value(0)+beta*(aH.coords.value(0)-sc.coords.value(0)));
    x1=aC.coords.value(0);

    if(Symp->num_of_coordinates>=2)
    {
        aC.coords.append(sc.coords.value(1)+beta*(aH.coords.value(1)-sc.coords.value(1)));
        x2=aC.coords.value(1);
        if(Symp->num_of_coordinates>=3)
        {
            aC.coords.append(sc.coords.value(2)+beta*(aH.coords.value(2)-sc.coords.value(2)));
            x3=aC.coords.value(2);
            if(Symp->num_of_coordinates>=4)
            {
                aC.coords.append(sc.coords.value(3)+beta*(aH.coords.value(3)-sc.coords.value(3)));
                x4=aC.coords.value(3);
                if(Symp->num_of_coordinates==5)
                {
                    aC.coords.append(sc.coords.value(4)+beta*(aH.coords.value(4)-sc.coords.value(4)));
                    x5=aC.coords.value(4);
                }
            }
        }
    }
    aC.value=expression.value();

    if(aC.value<aH.value)
    {
        aC.point_index=aH.point_index;
        Symp->iteration[iter+1][aH.point_index] = aC.coords;
        if(aC.coords[0]<Symp->min_x[0])
        {   Symp->min_x[0]=aC.coords[0];    }
        if(aC.coords[0]>Symp->max_x[0])
        {   Symp->max_x[0]=aC.coords[0];    }
        if(aC.coords[1]<Symp->min_x[1])
        {   Symp->min_x[1]=aC.coords[1];    }
        if(aC.coords[1]>Symp->max_x[1])
        {   Symp->max_x[1]=aC.coords[1];    }
        return 1;
    }
    else
    {   return 0; }
}

bool algorytm::contract_out()
{
    aC.coords.clear();

    aC.coords.append(sc.coords.value(0)+beta*(aR.coords.value(0)-sc.coords.value(0)));
    x1=aC.coords.value(0);
    if(Symp->num_of_coordinates>=2)
    {
        aC.coords.append(sc.coords.value(1)+beta*(aR.coords.value(1)-sc.coords.value(1)));
        x2=aC.coords.value(1);
        if(Symp->num_of_coordinates>=3)
        {
            aC.coords.append(sc.coords.value(2)+beta*(aR.coords.value(2)-sc.coords.value(2)));
            x3=aC.coords.value(2);
            if(Symp->num_of_coordinates>=4)
            {
                aC.coords.append(sc.coords.value(3)+beta*(aR.coords.value(3)-sc.coords.value(3)));
                x4=aC.coords.value(3);
                if(Symp->num_of_coordinates==5)
                {
                    aC.coords.append(sc.coords.value(4)+beta*(aR.coords.value(4)-sc.coords.value(4)));
                    x5=aC.coords.value(4);
                }
            }
        }
    }
    aC.value=expression.value();

    if(aC.value<aH.value)
    {
        aC.point_index=aH.point_index;
        Symp->iteration[iter+1][aH.point_index]=aC.coords;
        if(aC.coords[0]<Symp->min_x[0])
        {   Symp->min_x[0]=aC.coords[0];    }
        if(aC.coords[0]>Symp->max_x[0])
        {   Symp->max_x[0]=aC.coords[0];    }
        if(aC.coords[1]<Symp->min_x[1])
        {   Symp->min_x[1]=aC.coords[1];    }
        if(aC.coords[1]>Symp->max_x[1])
        {   Symp->max_x[1]=aC.coords[1];    }
        return 1;
    }
    else
    {   return 0; }
}

void algorytm::reduct()
{
    Sympleks::coordinate temp_coord;
    Sympleks::point temp_point;
    double aTemp;

    for (int i=0; i<Symp->num_of_points; i++)
    {
        temp_coord.clear();
        aTemp=aL.coords.value(0)+sigma*(Symp->iteration.value(iter).value(i).value(0)-aL.coords.value(0));
        temp_coord.append(aTemp);

        if(Symp->num_of_coordinates>=2)
        {
            aTemp=aL.coords.value(1)+sigma*(Symp->iteration.value(iter).value(i).value(1)-aL.coords.value(1));
            temp_coord.append(aTemp);

            if(Symp->num_of_coordinates>=3)
            {
                aTemp=aL.coords.value(2)+sigma*(Symp->iteration.value(iter).value(i).value(2)-aL.coords.value(2));
                temp_coord.append(aTemp);

                if(Symp->num_of_coordinates>=4)
                {
                    aTemp=aL.coords.value(3)+sigma*(Symp->iteration.value(iter).value(i).value(3)-aL.coords.value(3));
                    temp_coord.append(aTemp);

                    if(Symp->num_of_coordinates==5)
                    {
                        aTemp=aL.coords.value(4)+sigma*(Symp->iteration.value(iter).value(i).value(4)-aL.coords.value(4));
                        temp_coord.append(aTemp);
                    }
                }
            }
        }
        temp_point.append(temp_coord);
    }

    Symp->iteration[iter+1]=temp_point;
}
