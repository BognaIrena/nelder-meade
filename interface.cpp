#include "interface.h"
#include "ui_interface.h"

Interface::Interface(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Interface)
{
 //   resize(400, 400);
    ui->setupUi(this);
    setWindowTitle("Optymalizacja funkcji nieliniowej metodą Neldera-Meada");

    ui->StepSlider->setTracking(0);
    ui->StepSlider->setSingleStep(1);

    ui->IterationAmount->setRange(1,1000);
    ui->IterationAmount->setWrapping(true);
    ui->IterationAmount->stepBy(1);
    ui->raport->setReadOnly(true);
    ui->IterationAmount->setValue(200);
    ui->IterationAmount->setEnabled(true);
    ui->tabWidget->setTabText(0, "Wykres");
    ui->tabWidget->setTabText(1, "Raport");
    this->eps=ui->EditEpsilon->text().toDouble();


    for(int i=0; i<5; i++)
    {
        this->min_x_set[i]=false;
        this->max_x_set[i]=false;
    }


    connect(&okienko, SIGNAL(FunctionLoaded(std::string)),
            this, SLOT(on_FunctionLoaded(std::string)));
}

Interface::~Interface()
{
    delete ui;
}


void Interface::on_StepSlider_sliderMoved(int position)
{
    ui->IterCounter->display(position);
    if (Type==2)
    {
        QVector<double> x,y;

        for(int i=0; i<Symp->num_of_points; i++)
        {
            x1=this->Symp->iteration.value(position).value(i).value(0);
            x.append(x1);
            y.append(expression.value());
        }
        x1=this->Symp->iteration.value(position).value(0).value(0);
        x.append(x1);
        y.append(expression.value());
        qDebug() << "Współrzędne sympleksu:\n"
                 << x << endl <<y;

        ui->wykres->graph(1)->setData(x, y);
        ui->wykres->replot();
    }
    else if( Type==3)
    {
        QVector<double> x,y;
        for(int i=0; i<Symp->num_of_points; i++)
        {
            x.append(this->Symp->iteration.value(position).value(i).value(0));
            y.append(this->Symp->iteration.value(position).value(i).value(1));
        }
        x.append(this->Symp->iteration.value(position).value(0).value(0));
        y.append(this->Symp->iteration.value(position).value(0).value(1));
        qDebug() << "Współrzędne sympleksu:\n"
                 << x << endl <<y;
            ui->wykres->graph(1)->setData(x, y);
            ui->wykres->replot();
    }
}

void Interface::on_LoadFunction_clicked()
{
    ui->wykres->clearGraphs();
    ui->wykres->clearFocus();
    ui->wykres->clearPlottables();
    ui->wykres->clearItems();
    ui->wykres->replot();
    ui->raport->clear();
    okienko.exec();

}

void Interface::on_Start_clicked()
{
    ui->wykres->clearGraphs();
    ui->wykres->clearFocus();
    ui->wykres->clearPlottables();
    ui->wykres->clearItems();
    ui->raport->clear();


    for(int i=0; i<Symp->num_of_coordinates; i++)
    {
        if(!this->min_x_set[i])
        {   this->min_x[i]=-100;    }

        if(!this->max_x_set[i])
        {   this->max_x[i]=100;    }

    }
    // Wprowadzanie ograniczeń zakresu argumentów
        for(int i=0; i<Symp->num_of_coordinates; i++)
        {
            this->Symp->min_constraint[i]=this->min_x[i];
            this->Symp->max_constraint[i]=this->max_x[i];
        }

    qDebug()<<"Wprowadzono ograniczenia";
    Symp->initiate();

    this->min_search=new algorytm(ui->EditAlfa->text().toDouble(),
                                  ui->EditBeta->text().toDouble(),
                                  ui->EditGamma->text().toDouble(),
                                  ui->EditSigma->text().toDouble(),
                                  Symp, iter_amount);
    this->min_search->expr_string=this->expr_string;
    this->min_search->Nelder_Meade(eps);
    ui->raport->append(createLog());
    ui->StepSlider->setRange(0,this->min_search->iter);

    parsowanie();

    if(Type==2)
    {
        QVector<double> x,y;
        for(int i=0; i<Symp->num_of_points; i++)
        {
            x1=this->Symp->iteration.value(0).value(i).value(0);
            x.append(x1);
            y.append(expression.value());
        }
        x1=this->Symp->iteration.value(0).value(0).value(0);
        x.append(x1);
        y.append(expression.value());

            // create graph and assign data to it:
            ui->wykres->addGraph(ui->wykres->xAxis, ui->wykres->yAxis);
            ui->wykres->graph(1)->setPen(QPen(Qt::red));
            ui->wykres->graph(1)->setName("Sympleks");
            ui->wykres->graph(1)->setLineStyle(QCPGraph::lsNone);
            ui->wykres->graph(1)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, 5));
            ui->wykres->graph(1)->setData(x, y);
            ui->wykres->replot();
    }
    else if( Type==3)
    {
        QVector<double> x,y;
        for(int i=0; i<Symp->num_of_points; i++)
        {
            x.append(this->Symp->iteration.value(0).value(i).value(0));
            y.append(this->Symp->iteration.value(0).value(i).value(1));
        }
        x.append(this->Symp->iteration.value(0).value(0).value(0));
        y.append(this->Symp->iteration.value(0).value(0).value(1));

            // create graph and assign data to it:
            ui->wykres->addGraph(ui->wykres->xAxis, ui->wykres->yAxis);
            ui->wykres->graph(1)->setPen(QPen(Qt::white));
            ui->wykres->graph(1)->setName("Sympleks");
            ui->wykres->graph(1)->setLineStyle(QCPGraph::lsNone);
            ui->wykres->graph(1)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 5));
            ui->wykres->graph(1)->setData(x, y);
            ui->wykres->replot();
    }
    else
    {
        QMessageBox msgBox;
        msgBox.setText("Wymiarowość funkcji jest zbyt wysoka\ndo przedstawienia graficznego");
        msgBox.exec();
        ui->tab_2->focusWidget();
        ui->tab_1->hide();
        ui->tabWidget->setCurrentWidget(ui->tab_2);
    }
}


void Interface::on_Stop_clicked()
{
    ui->wykres->clearGraphs();
    ui->wykres->clearFocus();
    ui->wykres->clearPlottables();
    ui->wykres->clearItems();
    ui->raport->clear();
    ui->wykres->replot();
    this->Symp->iteration.clear();
}


void Interface::on_EditAlfa_textEdited(const QString &arg1)
{
    this->alfa=arg1.toFloat();
 //   this->Symp.a=arg1.toFloat();
}

void Interface::on_EditBeta_textEdited(const QString &arg1)
{
    this->beta=arg1.toFloat();
 //   this->Symp2D.b=arg1.toFloat();
}

void Interface::on_EditGamma_textEdited(const QString &arg1)
{
    this->gamma=arg1.toFloat();
//    this->Symp2D.c=arg1.toFloat();
}

void Interface::on_EditEpsilon_textEdited(const QString &arg1)
{
    this->eps=arg1.toFloat();
}

void Interface::on_IterationAmount_valueChanged(int arg1)
{
    this->iter_amount=arg1;
    ui->StepSlider->setRange(1,arg1);
}

void Interface::on_FunctionLoaded(std::string function)
{
  this->expr_string=function;
  QString func;

      if (func.fromStdString(function).contains("x5"))
      {
          this->Type=6;
          ui->min_x1->setValue(-100.0);
          ui->min_x1->setEnabled(true);
          ui->min_x2->setValue(-100.0);
          ui->min_x2->setEnabled(true);
          ui->min_x3->setValue(-100.0);
          ui->min_x3->setEnabled(true);
          ui->min_x4->setValue(-100.0);
          ui->min_x4->setEnabled(true);
          ui->min_x5->setValue(-100.0);
          ui->min_x5->setEnabled(true);

          ui->min_x1->setValue(100.0);
          ui->max_x1->setEnabled(true);
          ui->min_x2->setValue(100.0);
          ui->max_x2->setEnabled(true);
          ui->min_x3->setValue(100.0);
          ui->max_x3->setEnabled(true);
          ui->min_x4->setValue(100.0);
          ui->max_x4->setEnabled(true);
          ui->min_x5->setValue(100.0);
          ui->max_x5->setEnabled(true);

          qDebug()<<"Typ" <<Type;
      }
      else if (func.fromStdString(function).contains("x4"))
      {
          this->Type=5;
          ui->min_x1->setValue(-100.0);
          ui->min_x1->setEnabled(true);
          ui->min_x2->setValue(-100.0);
          ui->min_x2->setEnabled(true);
          ui->min_x3->setValue(-100.0);
          ui->min_x3->setEnabled(true);
          ui->min_x4->setValue(-100.0);
          ui->min_x4->setEnabled(true);

          ui->min_x1->setValue(100.0);
          ui->max_x1->setEnabled(true);
          ui->min_x2->setValue(100.0);
          ui->max_x2->setEnabled(true);
          ui->min_x3->setValue(100.0);
          ui->max_x3->setEnabled(true);
          ui->min_x4->setValue(100.0);
          ui->max_x4->setEnabled(true);

           qDebug()<<"Typ" <<Type;
      }
     else if (func.fromStdString(function).contains("x3"))
     {
      this->Type=4;
          ui->min_x1->setValue(-100.0);
          ui->min_x1->setEnabled(true);
          ui->min_x2->setValue(-100.0);
          ui->min_x2->setEnabled(true);
          ui->min_x3->setValue(-100.0);
          ui->min_x3->setEnabled(true);


          ui->min_x1->setValue(100.0);
          ui->max_x1->setEnabled(true);
          ui->min_x2->setValue(100.0);
          ui->max_x2->setEnabled(true);
          ui->min_x3->setValue(100.0);
          ui->max_x3->setEnabled(true);

      qDebug()<<"Typ" <<Type;
    }
    else if (func.fromStdString(function).contains("x2"))
    {
        this->Type=3;
          ui->min_x1->setValue(-100.0);
          ui->min_x1->setEnabled(true);
          ui->min_x2->setValue(-100.0);
          ui->min_x2->setEnabled(true);


          ui->min_x1->setValue(100.0);
          ui->max_x1->setEnabled(true);
          ui->min_x2->setValue(100.0);
          ui->max_x2->setEnabled(true);

        qDebug()<<"Typ" <<Type;
    }
    else if (func.fromStdString(function).contains("x1"))
    {
        this->Type=2;
          ui->min_x1->setValue(-100.0);
          ui->min_x1->setEnabled(true);

          ui->min_x1->setValue(100.0);
          ui->max_x1->setEnabled(true);

        qDebug()<<"Typ" <<Type;
    }
    else
    {
        QMessageBox msgBox;
        msgBox.setText("Błędnie wprowadzona formuła.\nNie znaleziono zmiennej x1.");
        msgBox.exec();
    }
      this->Symp = new Sympleks(Type-1);
}


/*!
 * \brief Interface::parsowanie funkcja wywoływana przy zatwierdzeniu wpisanej funkcji, odpowiada za przerobienie
 * wpisanego tekstu na wyrażenie matematyczne
 * \param expr tekst wpisany w okno
 */
void Interface::parsowanie()
{

    symbol_table.add_variable("x1",x1);
    if(Symp->num_of_coordinates>=2)
    {
        symbol_table.add_variable("x2",x2);
        if(Symp->num_of_coordinates>=3)
        {
            symbol_table.add_variable("x3",x3);
            if(Symp->num_of_coordinates>=4)
            {
                symbol_table.add_variable("x4",x4);
                if(Symp->num_of_coordinates==5)
                {
                    symbol_table.add_variable("x5",x5);
                }
            }
        }
    }

    symbol_table.add_constants();

    expression.register_symbol_table(symbol_table);

    parser.compile(this->expr_string, expression);

    if (this->Type==2)
    {
        double length=abs((Symp->min_x[0]-10) - (Symp->max_x[0]+10));
        double min_y=0;
        double max_y=0;
        QVector<double> x(1000);
        QVector<double> y(1000); // initialize with entries 0..100
        x[0] = x1= (Symp->min_x[0]-10);
        y[0] = expression.value();
        for (int i=1; i<1000; ++i)
        {
          //x[i] = (Symp->min_x[0]-10)+(i*length)/1000;
            x[i]=x[i-1]+length/1000;
          x1=x[i];
          y[i] = expression.value();

          if(y[i]<min_y)    { min_y=y[i]; }
          else if(y[i]>max_y)    { max_y=y[i]; }

        }
        // create graph and assign data to it:
        ui->wykres->addGraph();
        ui->wykres->setInteractions(QCP::iRangeDrag|QCP::iRangeZoom);
        ui->wykres->graph(0)->setData(x, y);
        // give the axes some labels:
        ui->wykres->xAxis->setLabel("x");
        ui->wykres->yAxis->setLabel("y");
        // set axes ranges, so we see all data:

        ui->wykres->xAxis->setRange(Symp->min_x[0]-10, Symp->max_x[0]+10);
        ui->wykres->yAxis->setRange(min_y, max_y);
        ui->wykres->replot();
    }

    if (this->Type==3)
    {
        int length1=abs((Symp->min_x[0]-10) - (Symp->max_x[0]+10));
        int length2=abs((Symp->min_x[1]-10) - (Symp->max_x[1]+10));
        ui->wykres->addGraph();
        // configure axis rect:
        ui->wykres->setInteractions(QCP::iRangeDrag|QCP::iRangeZoom); // this will also allow rescaling the color scale by dragging/zooming
        ui->wykres->axisRect()->setupFullAxesBox(true);
        ui->wykres->xAxis->setLabel("x1");
        ui->wykres->yAxis->setLabel("x2");

        // set up the QCPColorMap:
        QCPColorMap *colorMap = new QCPColorMap(ui->wykres->xAxis, ui->wykres->yAxis);
        ui->wykres->addPlottable(colorMap);
     //   int nx = 200;
     //   int ny = 200;
        colorMap->data()->setSize(500, 500); // we want the color map to have nx * ny data points
        colorMap->data()->setRange(QCPRange(Symp->min_x[0], Symp->max_x[0]),
                                   QCPRange(Symp->min_x[1], Symp->max_x[1])); // and span the coordinate range -4..4 in both key (x) and value (y) dimensions
        // now we assign some data, by accessing the QCPColorMapData instance of the color map:
        double y;
        for (int i=0; i<500; ++i)
        {
          for (int j=0; j<500; ++j)
          {
            colorMap->data()->cellToCoord(i, j, &x1, &x2);
            y=expression.value();
            colorMap->data()->setCell(i, j, y);
          }
        }

        // add a color scale:
        QCPColorScale *colorScale = new QCPColorScale(ui->wykres);
        ui->wykres->plotLayout()->addElement(0, 1, colorScale); // add it to the right of the main axis rect
        colorScale->setType(QCPAxis::atRight); // scale shall be vertical bar with tick/axis labels right (actually atRight is already the default)
        colorMap->setColorScale(colorScale); // associate the color map with the color scale
        colorScale->axis()->setLabel("Wartość w osi prostopadłej do ekranu");

        // set the color gradient of the color map to one of the presets:
        colorMap->setGradient(QCPColorGradient::gpPolar);
        // we could have also created a QCPColorGradient instance and added own colors to
        // the gradient, see the documentation of QCPColorGradient for what's possible.

        // rescale the data dimension (color) such that all data points lie in the span visualized by the color gradient:
        colorMap->rescaleDataRange();

        // make sure the axis rect and color scale synchronize their bottom and top margins (so they line up):
        QCPMarginGroup *marginGroup = new QCPMarginGroup(ui->wykres);
        ui->wykres->axisRect()->setMarginGroup(QCP::msBottom|QCP::msTop, marginGroup);
        colorScale->setMarginGroup(QCP::msBottom|QCP::msTop, marginGroup);

        // rescale the key (x) and value (y) axes so the whole color map is visible:
        ui->wykres->rescaleAxes();
    }

}

QString Interface::createLog()
{
    QString log, tmp;

    log+="Raport z przebiegu algorytmu\n";
    log+="Badana funkcja: %1";
    log+="\nWykonano %2 z %3 dozwolonych iteracji.\n";

    if(this->min_search->iter<=iter_amount)
    {
        log+="Minimum wynoszące %4 znaleziono w punkcie o współrzędnych %5.";
    }
    else
    {
        log+="Nie odnaleziono minimum ze względu na przekroczenie dozwolonej liczby iteracji.\n"
             "Spośród zbadanych punktów najmniejszy ma wartość %4 (współrzędne %5).\n";
    }
    tmp="[";
      for(int k=0; k<Symp->num_of_coordinates; k++)
      {
         tmp+="%1 ";
         tmp=tmp.arg(Symp->iteration.value(this->min_search->iter).value(this->min_search->aL.point_index).value(k));
      }
      tmp+="]\n";

    log=log.arg(tmp.fromStdString(expr_string)).arg(this->min_search->iter-1).arg(iter_amount).arg(this->min_search->aL.value).arg(tmp);

    log+="\nKolejne transformacje sympleksu:";
    for(int i=0; i<=this->min_search->iter; i++)
    {
        tmp="\nIteracja %1:\n";
        tmp=tmp.arg(i);
        for(int j=0; j<Symp->num_of_points; j++)
        {
            tmp+="\twierzchołek %1: [";
            tmp=tmp.arg(j);
            for(int k=0; k<Symp->num_of_coordinates; k++)
            {
                tmp+="%1 ";
                tmp=tmp.arg(Symp->iteration.value(i).value(j).value(k));
            }
            tmp+="]\n";
        }

        log+=tmp;
    }

    return log;
}


void Interface::on_min_x1_editingFinished()
{
    this->min_x_set[0]=true;
    this->min_x[0]=ui->min_x1->value();
}

void Interface::on_min_x2_editingFinished()
{
    this->min_x_set[1]=true;
    this->min_x[1]=ui->min_x2->value();
}

void Interface::on_min_x3_editingFinished()
{
    this->min_x_set[2]=true;
    this->min_x[2]=ui->min_x3->value();
}

void Interface::on_min_x4_editingFinished()
{
    this->min_x_set[3]=true;
    this->min_x[3]=ui->min_x4->value();
}

void Interface::on_min_x5_editingFinished()
{
    this->min_x_set[4]=true;
    this->min_x[4]=ui->min_x5->value();
}

void Interface::on_max_x1_editingFinished()
{
    this->max_x_set[0]=true;
    this->max_x[0]=ui->max_x1->value();
}

void Interface::on_max_x2_editingFinished()
{
    this->max_x_set[1]=true;
    this->max_x[1]=ui->max_x2->value();
}

void Interface::on_max_x3_editingFinished()
{
    this->max_x_set[2]=true;
    this->max_x[2]=ui->max_x3->value();
}

void Interface::on_max_x4_editingFinished()
{
    this->max_x_set[3]=true;
    this->max_x[3]=ui->max_x4->value();
}

void Interface::on_max_x5_editingFinished()
{
    this->max_x_set[4]=true;
    this->max_x[4]=ui->max_x5->value();
}





void Interface::on_EditSigma_textEdited(const QString &arg1)
{
        this->sigma=arg1.toFloat();
}
