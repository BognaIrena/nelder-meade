#include "parser.h"
#include "ui_parser.h"

Parser::Parser(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Parser)
{
    ui->setupUi(this);
    setWindowTitle(tr("Wprowadzanie funkcji"));
}

Parser::~Parser()
{
    delete ui;
}


void Parser::on_Help_clicked()
{
    Help suggest;
    suggest.exec();
}

void Parser::on_buttonBox_accepted()
{
    //parsowanie<double>(ui->NewFunction->text());
    emit(FunctionLoaded(ui->NewFunction->text().toStdString()));
    this->close();
}
